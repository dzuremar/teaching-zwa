function onLoad() {
  const clearFormButton = document.querySelector("input[type=button]");
  clearFormButton.addEventListener("click", confirmFormReset);
}

function confirmFormReset() {
  const isConfirmed = confirm("Opravdu vymazat formulář");
  if (isConfirmed) {
    const formElement = document.querySelector("form");
    formElement.reset();
  }
}

window.addEventListener("load", onLoad);
