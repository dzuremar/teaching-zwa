<?php
  function loadAccounts() {
    if (!file_exists("accounts.json")) {
      file_put_contents("accounts.json", '[]');
    }
    return json_decode(file_get_contents("accounts.json"), true);
  }

  function findAccount($email, $accounts) {
    foreach($accounts as $account) {
      if ($account["email"] == $email) {
        return $account;
      }
    }
  }

  function saveAccounts($accounts) {
    $fileContent = json_encode($accounts, JSON_PRETTY_PRINT);
    file_put_contents("accounts.json", $fileContent);
  }

  function printHeader() {
    if (isset($_SESSION["user"])) {
      echo "<header> Logged in as " . $_SESSION["user"]["email"];
      echo ' (<a href="logout.php">Logout</a>)</header>';
    }

    // ignore directories and query string
    $file = strtok(basename($_SERVER["REQUEST_URI"]), '?');

    if ($file != 'login.php') {
      echo '<a href="login.php">Login</a>';
      echo '<br>';
    }
    if ($file != 'register.php') {
      echo '<a href="register.php">Register</a>';
      echo '<br>';
    }
    if ($file != 'index.php' && $file != '') {
      echo '<a href="index.php">Home</a>';
      echo '<br>';
    }
    if (isset($_SESSION["user"]) && $_SESSION["user"]["isAdmin"]) {
      echo '<a href="users.php">Users</a>';
      echo '<br>';
    }
    echo '<br>';
  }
?>
