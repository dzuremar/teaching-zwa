<?php

  include "functions.php";

  session_start();

  $COMMENTS = "comments.json";

  // create comments file (empy JSON array) if it does not exist
  if (!file_exists($COMMENTS)) {
    file_put_contents($COMMENTS, "[]");
  }

  // read all comments as associative array from the JSON file
  $allComments = json_decode(file_get_contents($COMMENTS), true);

  if (isset($_POST["title"]) && isset($_POST["text"])) {
    // parse new comment from POST data
    $comment = array(
      "title" => $_POST["title"],
      "text" => $_POST["text"],
    );
    // add new comment to all comments
    array_push($allComments, $comment);
    // write all comments back to the JSON file
    file_put_contents($COMMENTS, json_encode($allComments));
    // redirect user to success page
    header("Location: commentSuccess.html");
  }
?>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Add comment</title>
  </head>
  <body>
    <?php printHeader() ?>
    <form method="post">
      <label for="title">Title</label>
      <input id="title" type="text" name="title" minlength="5" /><br />

      <label for="text">Text</label>
      <input id="text" type="text" name="text" minlength="10" /><br />

      <input type="submit" value="Add comment" />
    </form>
    <h1>Comments:</h1>
    <?php
      // initialize pagination variable from the query string
      $page = 0;
      if (isset($_GET["page"])) {
        $page = intval($_GET["page"]);
      } else {
        $page = $_GET["page"] = 0;
      }

      // render comments that belong to the specified page
      $pageComments = array_slice($allComments, $page * 5, 5);
      foreach ($pageComments as $comment) {
        echo "<h3>" . $comment["title"] . "</h3>";
        echo "<p>" . $comment["text"] . "</p>";
        echo "<hr/>";
      }

      // render "prev" button if this page is not the first one
      if ($page > 0) {
        echo "<button><a href=\"index.php?page=" . ($page - 1) . "\">prev</a></button>";
      }

      // render "next" button if this page is not the last one
      $remainingComments = count($allComments) - 5 * ($page + 1);
      if ($remainingComments > 0) {
        echo "<button><a href=\"index.php?page=" . ($page + 1) . "\">next</a></button>";
      }
    ?>
  </body>
</html>
