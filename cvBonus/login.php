<?php
  include "functions.php";

  session_start();

  $knownEmail = true;
  $correctPassword = true;

  if (isset($_POST["email"], $_POST["password"])) {
    $email = $_POST["email"];
    $password = $_POST["password"];
    $accounts = loadAccounts();
    $account = findAccount($email, $accounts);
    if (empty($account)) {
      $knownEmail = false;
    } else {
      if (password_verify($password, $account["password"])) {
        $_SESSION["user"] = $account;
        header('Location: loggedin.php');
      } else {
        $correctPassword = false;
      }
    }
  }
?>

<html>
  <head>
    <title>Login</title>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <?php printHeader() ?>
    <form method="post">
      <label for="email">email:</label>
      <input type="email" id="email" name="email" />
      <?php
        if (!$knownEmail) {
          echo "<span class='error'>Email not registered !</span>";
        }
      ?>
      <br>
      <label for="password">password:</label>
      <input type="password" id="password" name="password" />
      <?php
        if (!$correctPassword) {
          echo "<span class='error'>Incorrect password !</span>";
        }
      ?>
      <br>
      <input type="submit" value="Login!" />
    </form>
  </body>
</html>
